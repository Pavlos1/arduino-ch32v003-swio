all: main.elf

BOARD ?= UNO

CFLAGS = -DF_CPU=16000000 -Wall -Wextra -O1 -DBOARD_${BOARD}
AVRDUDE_FLAGS = -P /dev/ttyACM0 -Uflash:w:main.elf:e

ifeq (${BOARD}, UNO)
CFLAGS += -mmcu=atmega328p
AVRDUDE_FLAGS += -carduino -p m328p
endif

ifeq (${BOARD}, MEGA)
CFLAGS += -mmcu=atmega2560
AVRDUDE_FLAGS += -cwiring -p atmega2560 -D
endif

main.elf: *.c *.h
	avr-gcc $(CFLAGS) *.c -o main.elf

clean:
	rm -f main.elf

flash:
	avrdude ${AVRDUDE_FLAGS}

#ifndef SWIO_H
#define SWIO_H
#include <avr/io.h>
#include <util/delay.h>

#ifdef BOARD_UNO
#define SWIO_DDR  DDRB
#define SWIO_PORT PORTB
#define SWIO_PIN  PINB
#define SWIO_BIT  0
#endif

#ifdef BOARD_MEGA
#define SWIO_DDR  DDRH
#define SWIO_PORT PORTH
#define SWIO_PIN  PINH
#define SWIO_BIT  5
#endif

void swio_init();
void swio_write_reg(uint8_t addr, uint32_t val);
uint32_t swio_read_reg(uint8_t addr);

#endif
